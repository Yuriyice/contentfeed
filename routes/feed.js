/**
 * Created by yuriy on 8/25/14.
 */
var express = require('express');
var feedController = require('../controller/feedController');
var router = express.Router();


router.post('/', function (req, res) {
    feedController.createFeed(req, res);
});

router.put('/', function (req, res) {
    feedController.updateFeed(req, res);
});

router.get('/', function (req, res) {
    feedController.feeds(req, res);
});

router.delete('/', function (req, res) {
    feedController.deleteFeed(req, res);
});


module.exports = router;