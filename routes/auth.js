/**
 * Created by yuriy on 8/25/14.
 */

var express = require('express');
var userController = require('../controller/authController');
var router = express.Router();

router.post('/login', function (req, res) {
    userController.login(req, res);
});

router.post('/create', function (req, res) {
    userController.createUser(req, res);
});

router.post('/logout', function (req, res) {
    userController.logout(req, res);
});

router.post('/gcm', function (req, res) {
    userController.storeGCMKey(req, res);
});

module.exports = router;
