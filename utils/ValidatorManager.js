var Validator = require('validator');

/**
 * Email validation rules:
 * {min, max}
 */
var minEmailSymbols = 6;
var maxEmailSymbols = 32;

/**
 * Validate email. Return true if param valid, false - otherwise
 * @param emailCandidate - String email
 */
exports.isEmail = function (emailCandidate) {
    return emailCandidate
        && Validator.isEmail(emailCandidate)
        && Validator.isLength(emailCandidate, minEmailSymbols, maxEmailSymbols);
};

/**
 * Password validation rules
 * {min, max}
 */

var minPasswordSymbols = 6;
var maxPasswordSymbols = 18;

/**
 * Validate email
 * @param passwordCandidate
 * @returns validate in boolean
 */
exports.isPassword = function (passwordCandidate) {
    return passwordCandidate
        && Validator.isAlphanumeric(passwordCandidate)
        && Validator.isLength(passwordCandidate, minPasswordSymbols, maxPasswordSymbols);
};

exports.isPasswordMatch = function (password, repeatPassword) {
    return repeatPassword && Validator.equals(password, repeatPassword);
};

var minUsernameSymbols = 4;
var maxUsernameSymbols = 20;

exports.isUsername = function (username) {
    return username
        && Validator.isAlphanumeric(username)
        && Validator.isLength(username, minUsernameSymbols, maxUsernameSymbols);
};

exports.isObjectId = function(id) {
    try {
        return id && id.match(/^[0-9a-fA-F]{24}$/);
    } catch (e) {
        return false;
    }
};
