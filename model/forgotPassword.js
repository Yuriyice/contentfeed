var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var activation = mongoose.Schema({
    email: {type: String, unique: true, trim: true},
    username: {type: String, unique: true, trim: true},
    hashcode: String,
    expired: {type: Timestamp, default: Date.now() + (24 * 60 * 1000)}
});

activation.methods.generateHash = function () {
    return bcrypt.hashSync(Date.now + Math.random() + "SECRET", bcrypt.genSaltSync(8), null);
};

activation.methods.validHash = function (hash) {
    return hash == this.hashcode;
};

activation.methods.validDate = function (timestamp) {
    return this.expired >= timestamp;
};