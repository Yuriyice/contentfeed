exports.error = function (errorCode, errorMessage) {
    return {
        code: errorCode,
        message: errorMessage
    };
};

exports.emptyEmail = function () {
    return this.error(1, "Пустое поле email");
};

exports.emptyPassword = function () {
    return this.error(2, "Пустое поле password");
};

exports.wrongEmail = function () {
    return this.error(3, "Некорректный email");
};

exports.wrongPassword = function () {
    return this.error(4, "Некорректный password");
};

exports.serverInternalError = function () {
    return this.error(500, "Внутренняя ошибка сервера")
};

exports.invalidToken = function () {
    return this.error(5, "Требуются права администратора. Авторизируйтесь");
};

exports.userNotFound = function () {
    return this.error(6, "Пользователь не найден");
};

exports.invalidCredentials = function () {
    return this.error(7, "Неверная комбинация email/пароль")
};

exports.accessDenied = function () {
    return this.error(8, "Доступ к данной функции запрещён");
};

exports.invalidContent = function() {
    return this.error(9, "Контент пустой или менее 5 символов");
};

exports.notObjectId = function() {
    return this.error(10, "Переданный параметр не является ID")
};

exports.feedNotFound = function() {
    return this.error(11, "Новость не найдена");
};

exports.invalidGCMParams = function() {
    return this.error(12, "Неверные параметры GCM");
};

exports.noError = function() {
    return {
        status: "ok"
    }
};