var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var token = require('rand-token');

// define the schema for our user model
var userSchema = mongoose.Schema({
    email: {type: String, unique: true, trim: true},
    password: String,
    token: {type: String, trim: true},
    expireIn: {type: Number, default: Date.now() + (7 * 24 * 60 * 1000)},
    accessLevel: {type: Number}

});

// methods ======================
// generating a hash
userSchema.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};

userSchema.methods.refreshToken = function () {
    if (!this.isTokenValid()) return false;
    this.expireIn = Date.now() + (7 * 24 * 60 * 1000);
    return true;
};

userSchema.methods.generateToken = function() {
    return token.generate(16);
};


userSchema.methods.isTokenValid = function () {
    return this.expireIn > Date.now();
};

// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);
