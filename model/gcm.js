var mongoose = require('mongoose');

var gcmSchema = mongoose.Schema({
    deviceId: {type: String, unique: true},
    gcm: {type: String}
});

module.exports = mongoose.model("gcm", gcmSchema);