var mongoose = require('mongoose');

var feedSchema = mongoose.Schema({
    content: {type: String, trim: true},
    created: {type: Date, default: Date.now},
    updated: { type: Date, default: Date.now}
});

module.exports = mongoose.model('feed', feedSchema);