var gcm = require('../model/gcm');

var pushService = require('node-gcm-service');
var apikey = "AIzaSyB1nyjhmVmX1JULL4omu8M_lAmfSnB0sQ4";


exports.send = function (exceptToken, feed) {
    gcm.find({deviceId: {$ne: exceptToken}}, function (err, docs) {
            if (err) {
                console.log(err);
                return;
            }
            var arr = [];
            for (var g in docs) {
                if (g.gcm !== exceptToken) {
                    arr.push(g.gcm);
                }
            }
            var message = new pushService.Message({
                data: {
                    message: feed.content
                },
                delay_while_idle: true,
                time_to_live: 34,
                dry_run: false

            });

            var sender = new pushService.Sender();
            sender.setAPIKey(apikey);
            sender.sendMessage(message, arr, true, function (err, data) {
                console.log(arguments);
            });


        }
    )
    ;
}
;