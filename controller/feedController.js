var Error = require('../model/error');
var Feed = require('../model/feed');
var User = require('../model/user');
var Validator = require('../utils/ValidatorManager');
var GcmController = require('./gcmController');
var url = require('url');


exports.deleteFeed = function (req, res) {
    var token = req.headers['token'];

    if (!token) {
        return res.json(Error.invalidToken());
    }

    var urlParts = url.parse(req.url, true);
    var query = urlParts.query;
    if (!query) {
        return res.send(Error.notObjectId());
    }
    var feedId = query.feedId;
    if (!Validator.isObjectId(feedId)) {
        return res.send(Error.notObjectId());
    }

    User.findOne({"token": token}, function (err, user) {
        if (err) {
            return res.json(Error.serverInternalError());
        }
        if (!user) {
            return res.json(Error.userNotFound());
        }

        if (user.accessLevel < 200) {
            return res.json(Error.accessDenied());
        }

        if (!user.refreshToken()) {
            return res.json(Error.invalidToken());
        }
        Feed.remove({_id: feedId}, function (err, data) {
            if (err) return res.send(Error.serverInternalError());
            return res.send(Error.noError());
        });


    });

};

exports.createFeed = function (req, res) {
    var token = req.headers['token'];
    var deviceId = req.body.deviceId;
    if (!deviceId) {
        deviceId = "";
    }
    if (!token) {
        return res.json(Error.invalidToken());
    }
    User.findOne({"token": token}, function (err, user) {
        if (err) {
            return res.json(Error.serverInternalError());
        }
        if (!user) {
            return res.json(Error.userNotFound());
        }

        if (user.accessLevel < 200) {
            return res.json(Error.accessDenied());
        }

        if (!user.refreshToken()) {
            return res.json(Error.invalidToken());
        }

        var content = req.body.content;
        if (!content || content.length < 5) {
            return res.json(Error.invalidContent());
        }
        var feed = new Feed();
        feed.content = content;
        feed.save(function (err, data) {
            if (err || !data) {
                console.log(arguments);
                return res.json(Error.serverInternalError());
            }
            GcmController.send(deviceId, feed);
            return res.send(data.toJSON());
        });

    });

};

exports.updateFeed = function (req, res) {
    var token = req.headers['token'];
    if (!token) {
        return res.json(Error.invalidToken());
    }
    User.findOne({"token": token}, function (err, user) {
        if (err) {
            return res.json(Error.serverInternalError());
        }
        if (!user) {
            return res.json(Error.userNotFound());
        }

        if (user.accessLevel < 200) {
            return res.json(Error.accessDenied());
        }

        if (!user.refreshToken()) {
            return res.json(Error.invalidToken());
        }

        var content = req.body.content;

        if (!content || content.length < 5) {
            return res.json(Error.invalidContent());
        }

        var feedId = req.body.feedId;
        var moveToTop = req.body.top;
        if (!feedId || !Validator.isObjectId(feedId)) {
            return res.json(Error.notObjectId());
        }

        Feed.findById(feedId, function (err, data) {
            if (err) {
                return res.json(Error.serverInternalError());
            }
            if (!data) {
                return res.json(Error.feedNotFound());
            }
            data.content = content;
            delete data._id;

            var setData;
            if (moveToTop) {
                setData = {
                    content: content,
                    updated: Date.now()
                };
            } else {
                setData = {
                    content: content,
                    updated: data.updated
                };
            }

            Feed.update({_id: feedId}, {$set: setData}, {multi: false}, function (err, doc) {
                if (err || !doc) {
                    console.log(arguments);
                    return res.json(Error.serverInternalError());
                }
                return res.json(data);
            });
        });

    });

};

exports.feeds = function (req, res) {
    var urlParts = url.parse(req.url, true);
    var query = urlParts.query;

    var skip = 0;
    var limit = 0;

    var firstDate;
    var secondDate;

    if (query) {
        if (query.skip) {
            skip = query.skip > 0 ? query.skip : 0;
        }
        if (query.limit) {
            limit = query.limit > 0 ? query.limit : 50;
        }
        if (query.firstDate) {
            firstDate = query.firstDate;
        }
        if (query.lastDate) {
            secondDate = query.lastDate;
        }
    }

    var request = Feed
        .find(firstDate ? {"updated": {$gt: firstDate}} : secondDate ? {"updated": {$lt: secondDate}} : {})
        .limit(limit)
        .skip(skip)
        .sort({updated: 'desc'});

    request.exec(function (err, doc) {
        console.log(arguments);
        if (err) {
            return res.send(Error.serverInternalError());
        } else {
            return res.send(JSON.stringify({"content": doc}));
        }
    });
};

