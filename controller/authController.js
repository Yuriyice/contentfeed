var validator = require('../utils/ValidatorManager');
var Error = require('../model/error');
var User = require('../model/user');
var Gcm = require('../model/gcm');
var url = require('url');

exports.createUser = function (req, res) {
    var user = new User();
    user.password = user.generateHash("123321");
    user.email = "test@test.com";
    user.accessLevel = 255;
    user.save(null);
    return res.send(200);
};

exports.storeGCMKey = function (req, res) {

    var deviceId = req.body.deviceId;
    var gcmKey = req.body.gcm;

    if (!deviceId || !gcmKey) {
        return res.send(Error.invalidGCMParams());
    }

    var gcm = new Gcm();
    gcm.deviceId = deviceId;
    gcm.gcm = gcmKey;
    Gcm.update({deviceId: deviceId}, {$set: {gcm: gcmKey}}, {upsert: true}, function (err, result) {
        if (err || !result) {
            console.log(arguments);
            return res.send(Error.serverInternalError());
        }
        return res.send(Error.noError());
    });

};

exports.logout = function (req, res, next) {
    var token = req.headers['token'];
    if (token) {
        User.findOne({"token": token}, function (err, data) {
            if (err) {
                return res.send(Error.serverInternalError());
            }
            if (!data) {
                return res.send(Error.noError());
            }
            data.token = "";
            data.expireIn = 0;
            data.save(function (err, data) {
                if (err) return res.send(Error.serverInternalError());
                return res.send(Error.noError());
            });

        });
    } else {
        return res.send(200);
    }
};

exports.login = function (req, res, next) {
    var email = req.body.email;
    var password = req.body.password;
    if (!email) {
        return res.send(Error.emptyEmail());
    }
    if (!password) {
        return res.send(Error.emptyPassword());
    }

    if (!validator.isEmail(email)) {
        return res.send(Error.wrongEmail());
    }

    if (!validator.isPassword(password)) {
        return res.send(Error.wrongPassword());
    }

    User.findOne({"email": email}, function (err, user) {
        if (err) {
            console.log(err);
            return res.send(Error.serverInternalError());
        }

        if (!user) {
            return res.send(Error.userNotFound());
        }

        if (!user.validPassword(password)) {
            return res.send(Error.invalidCredentials());
        }

        user.token = user.generateToken();
        user.expireIn = Date.now() + (7 * 24 * 60 * 1000);
        user.save(function (err, data) {
            if (err) {
                return res.send(Error.serverInternalError());
            }
            user.password = "";
            return res.send(user.toJSON());
        });

    });

};